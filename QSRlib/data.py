
# this file contains main data struct of events

class QSR_structure:
    def __init__(self, t, type, obj1, obj2):
        self.time = t
        self.rcc = type
        self.obj1 = obj1
        self.obj2 = obj2
        self.status = True


class Event:
    def __init__(self, t, rcc, obj1, obj2):
        self.start_time = t
        self.end_time = t
        self.rcc = rcc
        self.obj1 = obj1
        self.obj2 = obj2
        self.status = True
        self.id= -1
        self.order=-1
        self.group=-1

# id=0
class Group:
    def __init__(self,obj1,obj2,id):
        self.id=id
        self.obj1=obj1
        self.obj2=obj2
        self.order=0
        self.member=[]

class Temporal_relation:
    def __init__(self):
        self.before=[]
        self.overlap=[]
        self.during=[]
        self.start=[]

    # def print_self(self):

