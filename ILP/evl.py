# this file is used to calculate the confusion matrix for 4 cross validation 


# matrix = [[4, 1, 0, 0, 0, 1], [1, 11, 0, 1, 0, 0], [1, 0, 11, 2, 0, 2], [1, 0, 2, 7, 0, 2], [1, 0, 2, 4, 8, 1]]
# matrix =[[4,1,0],[1,11,0],[1,0,11]]
# matrix =[[6,4,0,0],[2,12,0,0],[2,2,11,2]] #ilasp

# matrix = [[2, 1, 0, 0, 0, 4], [1, 6, 0, 0, 0, 6], [0, 0, 16, 0, 0, 0], [2, 2, 0, 6, 0, 4], [1, 2, 0, 0, 7, 3]]  # dis
# matrix = [[5, 1, 0, 0, 0, 1], [12, 10, 0, 0, 0, 0], [2, 0, 10, 0, 1, 4], [2, 0, 9, 7, 8, 0],[1, 0, 9, 7, 11, 0]]  # notmp

# matrix = [[6, 0, 0, 0, 0, 0], [1, 11, 0, 0, 0, 0], [0, 0, 15, 1, 0, 0], [1, 0, 3, 6, 2, 0],[1, 0, 0, 4, 7, 0]]  # decision
matrix = [[2, 4, 0, 0, 0, 0], [2, 10, 0, 0, 0, 0], [1, 0, 13, 2, 0, 0], [0, 0, 3, 4, 5, 0], [2, 0, 0, 4, 6, 0]]

# matrix = [[4, 1, 0, 1], [1, 11, 0, 0], [1, 0, 11, 4]]
# matrix = [[1, 0, 0, 0, 0, 5], [0, 9, 0, 0, 0, 3], [1, 0, 10, 0, 0, 5], [1, 0, 1, 4, 0, 6], [1, 0, 2, 0, 5, 4]]  # no kal

dim = len(matrix[0]) - 1
tp = [0 for _ in range(dim)]
row = [0 for _ in range(dim)]
col = [0 for _ in range(dim)]
recall = 0
accuary = 0
precision=0
for i in range(dim):
    tp[i] = matrix[i][i]
    for k in range(dim + 1):
        row[i] += matrix[i][k]
    for m in range(dim):
        col[i] += matrix[m][i]

sum = 0
for i in row:
    sum += i

# print()
for i in range(dim):
    accuary += tp[i] / sum
    precision+=tp[i] / col[i]
    recall += tp[i] / row[i]
accuary = accuary
recall = recall/dim
precision = precision/dim
print(accuary)
print(recall)
print(precision)
