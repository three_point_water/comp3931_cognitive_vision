# COMP3931  
# Relational Learning of Activity Based on Inductive Logic Programming  
The limits of my language means the limits of my world.  
----Ludwig Wittgenstein 'Tractatus Logico-Philosophicus'

# Datset: CAD-120
Please download dateset before running the program,  
http://pr.cs.cornell.edu/humanactivities/data.php 

# Requirement:  
Python 3.6  
Numpy  
sklearn  
Clyngor  
matplotlib(for visualization only)  

# Other Requirement:
QSRlib: https://qsrlib.readthedocs.io/en/latest/rsts/handwritten/install.html (also need python-igraph)  
gringo (need libpython2.7)  
Graphviz(for visualization only)  
IPython(for visualization only)  

# How to run:
1. abstruct the video data to qualitative data:  
main_script.py(rcc-3 and temporal relations)  
distance_script.py (distance and temporal relations)  
ml_script.py   
(re-implementation of paper Qualitative and Quantitative Spatio-Temporal Relations in Daily Living Activity Recognition )  

2. ILP methods  
after getting events and temporal relations, run files:  
ilp_tester_alt.py (for version 1)  
ilp_tester.py (for version 2)  


# Warning
The model of this project cannot be used in Commercial activities.  
