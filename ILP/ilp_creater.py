

# this file is used to test hypo for ilasp

import sys
from tree          import *
from queue_search  import *

from ILP.utils import read
from ilp_hypo  import *
# from data import Event,Group


def filter_same_results():
    final_result=[]
    for i,example in enumerate(results):
        for id,rules in reversed(list(enumerate(example))):
            match_list = [[] for _ in range(3)]
            for rule in rules:
                e=events[i][rule]
                match_list[e.group].append(e.rcc)
            if match_list in final_result:
                del results[i][id]
            else:
                final_result.append(match_list)

address="result/meta/"
head="making_cereal"
f_events = open(address+head+"_events.txt", "r")
f_groups = open(address+head+"_groups.txt", "r")
f_rels = open(address+head+"_relations.txt", "r")
# f_meta=eval(open("meta_group.txt","r").readline())

# print(f_meta)
# exit()
events,groups,relations=[],[],[]
read(f_events,0,events)
read(f_rels,1,relations)
read(f_groups,2,groups)
results=[[] for _ in range(len(events))]

print(len(events))
def zero_heuristic(state):
    return 0

for i in range(len(events)):
    search(make_ilp_problem(e=events, g=groups, id=i,re=results[i]), ('A_star', zero_heuristic), 5000, [])

filter_same_results()
def rcc_decode(id):
    return {
        0: "dc",
        1: "po",
        2: "o",
    }.get(id, -1)

def allen_decode(index):
    return {
        1:"before",
        4:"overlap",
        2:"start",
        3:"during",
    # index+1 since the for loop start from 0
    }.get(index,-1)

def pre(length):
    prefix=""
    prefix+=str(length)+" ~ "
    return prefix

def output(head):
    var="V"
    f = open("result/pl/" + head + "/hypo.txt", "w")
    for id,rules in enumerate(results):
        for rule in rules:
            length_rule=1
            # obj_names = ""
            last_var=rule[-1]
            first_var=rule[0]
            # for obj in range(len(meta)):
            #     obj_names += "V" +str(last_var+obj+1)+","
            # # sur = "cereal("+obj_names+ "V"+str(first_var)+",V"+str(last_var)+"):-"
            # sur = "cereal(" + "V" + str(first_var) + ",V" + str(last_var) + "):-"
            sur = head + "(" + var + str(first_var) + ",V" + str(last_var) + "):-"
            content=""
            # spatial relations
            for e in rule:
                event=events[id][e]
                if content!="":
                    content+=","
                # obj_name=""
                # for obj in meta[event.group]:
                #     obj_name+="V"+str(last_var+obj+1)+","
                # content+=rcc_decode(event.rcc)+"("+obj_name+var+str(e)+")"
                obj1 = event.obj1.title()
                obj2 = event.obj2.title()
                content += rcc_decode(event.rcc) + "(" + obj1 + "," + obj2 + "," + var + str(e) + ")"
                length_rule+=1
            # temporal relations
            for rel_id,rel in enumerate(relations[id]):
                for rel_list in rel:
                    if rel_list[0] in rule and rel_list[1] in rule:
                        content+=","+allen_decode(rel_id+1)+"("+var+str(rel_list[0])+","+var+str(rel_list[1])+")"
                        length_rule+=1
            content+="."
            prefix = pre(length_rule)
            content=prefix+sur+content
            f.writelines(content+"\n")

            # print(sur+content)


output(head)



#
#
