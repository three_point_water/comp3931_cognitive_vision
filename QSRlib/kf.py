
# this file is the abstruct class of kalman filter

import numpy as np
from abc import ABCMeta, abstractmethod


class EKF(object):
    __metaclass__ = ABCMeta

    def __init__(self, n, m, pval=0.1, qval=1e-4, rval=0.1):
        # no previous prediction noise covariance
        self.P_pre = None

        # Creates a KF object with n states, m observables, and specified values for
        # prediction noise covariance pval, process noise covariance qval, and
        # measurement noise covariance rval.
        # current state is zero, with diagonal noise covariance matrix
        self.x = np.zeros((n, 1))
        self.P_post = np.eye(n) * pval

        # get state transition and measurement Jacobians from implementing class
        self.F = self.getF(self.x)
        self.H = self.getH(self.x)

        # set up covariance matrices for process noise and measurement noise
        self.Q = np.eye(n) * qval
        self.R = np.eye(m) * rval

        # Identity matrix will be usefel later
        self.I = np.eye(n)

    def step(self, z):

        # Predict
        self.x = self.f(self.x)
        self.P_pre = self.F * self.P_post * self.F.T + self.Q

        # Update
        G = np.dot(self.P_pre * self.H.T, np.linalg.inv(self.H * self.P_pre * self.H.T + self.R))
        self.x += np.dot(G, (np.array(z) - self.h(self.x).T).T)
        self.P_post = np.dot(self.I - np.dot(G, self.H), self.P_pre)

        # return self.x.asarray()
        return self.x

    @abstractmethod
    def f(self, x):

        raise NotImplementedError()

    @abstractmethod
    def getF(self, x):

        raise NotImplementedError()

    @abstractmethod
    def h(self, x):

        raise NotImplementedError()

    @abstractmethod
    def getH(self, x):

        raise NotImplementedError()