
# this file is used to do the queue search and BFS

from __future__ import print_function

from random import *   # used for randomised depth first search
from tree import   *   # defines tree data structure
import time            # used to determine runtime
import sys             # used to flush the output buffer


global OPTIONS

def search(problem, strategy, max_nodes, options ):
    initialise_func    = problem[0]
    problem_info_func  = problem[1]
    initial_state      = problem[2]
    poss_act_func      = problem[3]
    successor_func     = problem[4]
    goal_test_func     = problem[5]
    global OPTIONS
    OPTIONS            = options

    if not(initialise_func == None):
       # Call function to initialise problem
       initialise_func()  # Call function to initialise problem


    problem_info_func()  # Call function to print problem info

    start_time = time.clock()

    node_queue = get_initial_node_queue( initial_state )
    for nodes_tested in range(max_nodes):
          ##print_node_queue(node_queue)

          if node_queue == []:
             print( "Time taken =", time.clock() - start_time, "seconds\n" )

             # print(action_path)
             return False
          first_node = node_queue.pop(0)  #take 1st node from queue

          #if apply(goal_test_func, [node_get_state( first_node )] ):
          if goal_test_func( node_get_state( first_node ) ):
             action_path = node_get_path(first_node)

             print_action_list(action_path)
             print( "Total nodes tested = " + str(nodes_tested+1) )
             print( "Time taken =", time.clock() - start_time, "seconds\n" )
             return action_path
          children = node_get_children(node_expand( first_node,
                                                    poss_act_func,
                                                    successor_func
                                                  )
                                      )
          node_queue = add_to_node_queue(strategy, node_queue, children)
    print( "Time taken =", time.clock() - start_time, "seconds\n" )
    return False

def get_initial_node_queue( initial_state ):
    return [node_set_state( new_node(), initial_state )]


def node_expand(node, poss_actions, successor_fun ):

      state = node_get_state( node )
      path  = node_get_path( node )
      suc_pairs = possible_action_successor_pairs(state, poss_actions,
                                                         successor_fun)
      for suc_pair in suc_pairs:
          action = suc_pair[0]
          result_state = suc_pair[1]
          child = new_node()
          node_set_parent( child, node )
          node_set_path( child, path + [action] )
          node_set_state( child, result_state )
      return node


def possible_action_successor_pairs(state, poss_actions, successor_fun ):
       ##poss_acts = apply( poss_actions, [state] )
       poss_acts = poss_actions( state )
       ##return [(action, apply(successor_fun, (action,state))) for action in poss_acts]
       return [(action, successor_fun(action,state)) for action in poss_acts]


def add_to_node_queue( strategy, node_queue, new_nodes ):
        if (strategy == 'breadth_first'):
           return node_queue + new_nodes
        if (strategy == 'depth_first'):
           return new_nodes + node_queue
        if (strategy == 'randomised_depth_first'):
           shuffle(new_nodes)
           return new_nodes + node_queue
        print( "ERROR: unknown strategy: " + strategy )


### Need to update and modularise this part of the code
### Really it needs just one "weight" value stored on each node
### The add_nodes function should be passed the weight function.


#### A few handy functions ####

def print_node_queue(node_queue):
     print( "node_queue" )
     for n in node_queue:
         print( node_get_state(n) )


def print_action_list( act_list ):
     print( ", ".join([action_string(action) for action in act_list]) )


def action_string( action ):
          return str(action)
