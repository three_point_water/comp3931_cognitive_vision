# -*- coding: utf-8 -*-
"""
this file is used to do ilp task for version 1
"""

from __future__ import print_function

from copy import deepcopy


def ilp_print_problem_info():
    print("starting ")

# intial state all examples can be corvered
def ilp_get_initial_state():
    return [0, [],{}]


# return a list
def ilp_possible_actions(state):
    if state[0] == 0:
        return ilp_initial_addition()
    else:
        return ilp_following_addition(state)


# all possible next literal
def ilp_initial_addition():
    next = [i for i in range(length_event)]
    return next


# dc 0
# po 1
# o  2
def ilp_following_addition(state):
    current_id = state[1][-1]
    next = []
    current_event = events[current_id]
    if current_id + 1 < length_event:
        for i in range(current_id + 1, length_event):
            next_event = events[i]
            next_group = next_event.group
            # if in the same group, compare directly
            if current_event.group == next_group:
                if current_event.rcc + 1 == next_event.rcc \
                        or current_event.rcc - 1 == next_event.rcc:
                    next.append(i)
            # if not, seach for the node with different group
            else:
                # if the next event already first one in the group
                if next_event.order == 0:
                    next.append(i)
                # if not, find the previous event of next event
                else:
                    max_order=-1
                    for idx in state[1]:
                        e=events[idx]
                        if e.group==next_group:
                            if e.order>max_order:
                                max_order=e.order
                    rcc = groups[next_group].member_alt[max_order]
                    if rcc + 1 == next_event.rcc or rcc - 1 == next_event.rcc:
                        next.append(i)

    return next


def find_index(word, list):
    return [i for i, c in enumerate(list) if c == word]


def create_rule(rule):
    var = "V"
    last_var = rule[-1]
    first_var = rule[0]
    sur = head+"(" + "V" + str(first_var) + ",V" + str(last_var) + "):-"
    content = ""
    # spatial relations
    for e in rule:
        event = events[e]
        if content != "":
            content += ","
        content += rcc_decode(event.rcc) + "(" +str(event.obj1).title() +"," +\
                   str(event.obj2).title()+","+var + str(e) + ")"
    # temporal relations
    for rel_id, rel in enumerate(relations):
        for rel_list in rel:
            if rel_list[0] in rule and rel_list[1] in rule:
                content += "," + allen_decode(rel_id + 1) + "(" + var + str(rel_list[0]) \
                           + "," + var + str(rel_list[1]) + ")"
    content += "."
    return sur+content

def test(state):
    rule=create_rule(state[1])
    for i in test_dic.keys():
        if state[2][i]==1:
            test_script=test_dic[i]+rule
            answers = ASP(test_script)
            flag=1
            for answer in answers:
                # print(answer)
                for key in answer:
                    # print(key[0])
                    if head in key[0]:
                        flag=0
                        break
                if not flag:
                    break
            if flag:
            # if can reach here, no solution
                state[2][i]=0



def ilp_initial_successor(action):
    new_state = deepcopy(ilp_initial_state)
    # update action first
    new_state[0]+=1
    new_state[1].append(action)
    for i in test_dic.keys():
        new_state[2][i]=1
    return new_state

# find next states
def ilp_successor_state(action, state):
    if state[0] == 0:  # initial state
        return ilp_initial_successor(action)
    new_state = deepcopy(state)
    new_state[1].append(action)
    new_state[0] += 1
    return new_state


# in fact, there is no goal
def ilp_goal_state(state):
    sum=0
    for i in state[2].values():
        sum+=i
    # a naive filtering to decrease calcuate in post process
    if state[0]>2:
        if sum > len(state[2])/2+1:
            result.append(state[1])
    return False

import os
def initial_test_file():
    test_dic={}
    # root dir are result, dis,kal and notmp
    rootDir = "result_kal/pl/"+head
    for lists in os.listdir(rootDir):
        path = os.path.join(rootDir, lists)
        f = open(path, "r")
        fp = f.readlines()
        content = ""
        for c in fp:
            content += c + "\n"
        key=''.join(list(os.path.basename(path))[:-3])
        test_dic[key]=content
    return test_dic

def make_ilp_problem(e, g, id,re,h,r):
    global events, groups, ilp_initial_state, target,result,length_event,test_dic,head,relations
    head=h
    target = id
    events = e[target]
    groups = g[target]
    relations=r[target]
    length_event = len(events)
    result=re
    ilp_initial_state = ilp_get_initial_state()
    test_dic=initial_test_file()
    return (None,
            ilp_print_problem_info,
            ilp_initial_state,
            ilp_possible_actions,
            ilp_successor_state,
            ilp_goal_state
            )
def rcc_decode(id):
    return {
        0: "dc",
        1: "po",
        2: "o",
    }.get(id, -1)
def dis_decode(id):
    return {
        0: "short",
        1: "medium",
        2: "long",
    }.get(id, -1)
def allen_decode(index):
    return {
        1:"before",
        4:"overlap",
        2:"start",
        3:"during",
    # index+1 since the for loop start from 0
    }.get(index,-1)
