#!/usr/bin/env python
# -*- coding: utf-8 -*-

# this file is used to generate distance relations


from __future__ import print_function, division
import argparse
from data import QSR_structure,Event
import os
from preprocess import Read_CAD_obj,kal
from writeILP import write_files,write_ILP,write_pl
import itertools
import math
from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt

def obj_form(obj_list):
    out = []
    for i in itertools.combinations(obj_list, 2):
        out.append(list(i))
    return out


def output_world_qsr_trace(obj_list,fn,sub_name,activity,output):
    events=[]
    id = 0
    for i,c in enumerate(output):
        if c.status:
            event=Event(c.time,c.rcc,c.obj1,c.obj2)
            for k in range(len(output)):
                if output[k].status:
                    # if next relation is same as this one, merge together
                    if (output[k].time==event.end_time+1)&(output[k].obj1==event.obj1)&(output[k].obj2==event.obj2)\
                            &(output[k].rcc==event.rcc):
                        event.end_time=output[k].time
                        output[k].status=False
            event.id=id
            id+=1
            events.append(event)


    # total frames
    frames = events[-1].end_time
    # events number
    length=len(events)

    # this list is used to record the unique id of time interval
    unique_time=[]
    # first to find any unique time interval,
    for i, c in enumerate(events):
        if c.status:
            for k in range(length):
                if i!=k and events[k].start_time==c.start_time and events[k].end_time==c.end_time:
                    events[k].id=c.id
                    events[k].status=False
            unique_time.append(c.id)


    thresold1=max(frames/length*0.3,15)
    thresold2=frames/length*0.35
    print(thresold1)
    # for i in events:
    #     print(i.id, i.start_time, i.end_time, i.obj1, i.obj2, i.rcc)
    # merge too small events
    flag = 1
    while flag:
        events, flag = merge(events, thresold1)
        length = get_correct_id(events)
        thresold1 = max(15,frames / length * 0.3)
        # if thresold1<10:
        #     thresold1=10


    keep=999
    for i in events:
        # print(i.id,i.start_time,i.end_time,i.obj1,i.obj2,i.rcc)
        if abs(i.start_time-i.end_time)<keep:
            keep=abs(i.start_time-i.end_time)
    print(keep)
    # exit()

    # reID
    events = sorted(events,key=lambda i:i.start_time,reverse=True)
    events = sorted(events, key=lambda i: i.end_time)

    # get_correct_id(events)
    for k in range(len(events)):
        events[k].id = k

    groups = []
    restruct(groups, events, obj_list)

    for i in events:
        print(i.id,i.start_time,i.end_time,i.obj1,i.obj2,i.rcc)
    # return

    length=get_correct_id(events)

    # create empty list with length 4
    relations = [[] for _ in range(4)]
    for i, c in enumerate(events):
        # record the forbidden groups
        non_group_id=[]
        for k in range(i+1,length):
            tmp=0
            if events[k].group in non_group_id:
                continue
            # if in same group
            if c.group==events[k].group:
                # if idx+1<idx2 , jump to next one
                if c.order+1<events[k].order:
                    continue
                #  must be smaller than
                elif c.order==events[k].order:
                    print("error")
                    print(c.order+1,events[k].order)
                else:
                    tmp = allen_encode(c.start_time, c.end_time, events[k].start_time, events[k].end_time)

            # two groups are not same
            else:
                tmp = allen_encode(c.start_time, c.end_time, events[k].start_time, events[k].end_time)
                non_group_id.append(events[k].group)

            # handle error
            if not tmp:
                print("error happens\n")
                print(str(i)+" "+str(k))

            else:
                relations[tmp-1].append([c.id,events[k].id])

    relations=post(relations)

    for idx,c in enumerate(events):
        rcc=c.rcc
        groups[c.group].member_alt.append(rcc)


    # write_ILP(events, relations, unique_time, fn, sub_name,activity,obj_list)
    write_files(events, relations, groups,activity)
    write_pl(events, relations,sub_name,activity)


# this method is to merge some events which length is too small (threshod)
def merge(events,threshod):
    for idx, c in enumerate(events):
        if abs(c.start_time-c.end_time)<threshod:
            previous_id=-1
            next_id=99
            for idx1 in range(0,idx):
                if events[idx1].obj1==c.obj1 and events[idx1].obj2==c.obj2:
                    if idx1>previous_id:
                        previous_id=events[idx1].id

            for idx2 in range(idx+1,len(events)):
                if events[idx2].obj1==c.obj1 and events[idx2].obj2==c.obj2:
                    if idx2<next_id:
                        next_id=events[idx2].id
            # the current event is the first event in the group
            if previous_id==-1 and next_id!=99:
                next_next_id=99
                # find next next id
                for idx3 in range(next_id + 1, len(events)):
                    if events[idx2].obj1 == c.obj1 and events[idx2].obj2 == c.obj2:
                        if idx2 < next_next_id:
                            next_next_id = events[idx2].id
                # if no next next event
                if next_next_id==99:
                    c.end_time = events[next_id].end_time
                    del events[next_id]
                    return events, 1

                else:
                    # if next next event rcc != current event rcc
                    # not merge next event
                    if events[next_next_id].rcc!=c.rcc:
                        continue
                    else:
                        # expend the current event's end time
                        c.end_time=events[next_next_id].end_time
                        del events[next_id]
                        del events[next_next_id]
                        return events, 1
            # the current event is the middle event in the group
            elif previous_id!=-1 and next_id!=99:
                # if previous event is dc current is po next is o
                # the current cannot be merged
                if events[previous_id].rcc == events[next_id].rcc:
                    events[previous_id].end_time = events[next_id].end_time
                    # delete from last one to avoid index error
                    # delete next id and current id
                    del events[next_id]
                    del events[idx]
                    return events, 1
                elif int(events[previous_id].rcc)+1==int(events[next_id].rcc) \
                    or int(events[previous_id].rcc)-1==int(events[next_id].rcc):
                    events[previous_id].end_time = events[idx].end_time
                    del events[idx]
                    return events,1
            # the current event is the last event in the group
            elif previous_id!=-1 and next_id==99:
                previous_previous_id = -1
                # find  previous previous event
                for idx4 in range(0, previous_id):
                    if events[idx4].obj1 == c.obj1 and events[idx4].obj2 == c.obj2:
                        if idx4 < previous_previous_id:
                            previous_previous_id = events[idx4].id
                # if no previous previous event
                if previous_previous_id == -1:
                    events[previous_id].end_time = c.end_time
                    # delete current event
                    del events[idx]
                    return events, 1
                else:
                    # if previous previous event rcc != current event rcc
                    # not merge current event
                    if events[previous_previous_id].rcc != c.rcc:
                        continue
                    else:
                        # expend the previous previous event's end time
                        events[previous_previous_id].end_time = c.end_time
                        del events[idx]
                        del events[previous_id]
                        return events, 1

    # all is fine. return 0 as a flag
    return events,0

# find the relation which is still all the time  (threshod)
def discard(events,frames,groups,threshod):
    # get the length of whole activiyty

    for idx,c in enumerate(events):
        # if the length of event is longer than threshod, we regard it still all the time
        if abs(c.start_time-c.end_time)>frames-threshod:
            # record the deleted event
            print(c.obj1, c.obj2, c.start_time, c.end_time, c.rcc)
            # delete all members in the same group
            # list = groups[c.group].member
            # revese the list to avoid index error
            group_id=c.group
            for i in reversed(range(len(events))):
                if events[i].group==group_id:
                    del events[i]
            return 1
    return 0


def get_correct_id(events):
    length=len(events)
    for i in range(length):
        events[i].id=i
    return length


def post(relations):
    flag=1
    while flag:
        relations,flag=delete_trans(relations)
    return relations

# except overlap
# find transitive relations
# if (x,y) (x,z) and (y,z), then discard (x,z)
def delete_trans(relations):
    for i in range(1, 3):
        for idx, c in enumerate(relations[i]):
            x = c[0]
            y = c[1]
            for k in range(idx + 1, len(relations[i]) - 1):
                if relations[i][k][0] == x and relations[i][k][1] != y:
                    z = relations[i][k][1]
                    if [y, z] in relations[i]:
                        # del_list.append([x,z])
                        del relations[i][k]
                        return relations,1
    return relations, 0


# input: id of time interval
# retrun: id of groups
def find_group(id,groups):
    for i in groups:
        if id in i.member:
            return i.id

from data import Group
def restruct(groups, events, obj_list):
    id=0
    # create order struct
    for i in range(len(obj_list)):
        # avoid repeated search
        for k in range(i+1,len(obj_list)):
            # create groups
            groups.append(Group(obj_list[i], obj_list[k], id))
            id+=1

    for idx,i in enumerate(events):
        for idx2,k in enumerate(groups):
            if (i.obj1==k.obj1 and i.obj2==k.obj2) or (i.obj1==k.obj2 and i.obj2==k.obj1):
                i.order=k.order
                i.group=k.id
                k.order+=1
                k.member.append(idx)


# create allen interval relations
# just show clearly, but is not efficient
# relation->index
# t1 t2 : start and end time of event 1
# t3 t4 :                    of event 2
def allen_encode(t1, t2, t3, t4):
    # before and meet
    if t2<=t3:
        return 1
    # start
    elif t1==t3 and t2<t4:
        return 2
    # during and finish
    elif t3<t1 and t2<=t4:
        return 3
    # overlap
    elif t2 > t3 and t2 <= t4:
        return 4

    else:
        return 0


# create allen interval relations
# just show clearly, but is not efficient
# relation->index
# t1 t2 : start and end time of event 1
# t3 t4 :                    of event 2
def allen_encode_2(t1, t2, t3, t4):
    # before
    if t2<t3:
        return 1

    # meet
    elif t2==t3:
        return 2

    # overlap
    elif t2>t3& t2<t4:
        return 3

    # start
    elif t1==t3 & t2<t4:
        return 4

    # during
    elif t1>t3 & t2<t4:
        return 5

    # finish
    elif t1>t3 & t2==t4:
        return 6

    # equal
    elif t1==t3 & t2==t4:
        return 7

    else:
        return 0

def cluster(list):
    X=np.array(list).reshape(-1,1)
    method=KMeans(n_clusters=3)
    y=method.fit_predict(X)
    # print(y)
    print(method.cluster_centers_)


if __name__ == "__main__":
    activity_list=["unstacking_objects","cleaning_objects","stacking_objects","cleaning_objects","microwaving_food"]
    # activity_list=["unstacking_objects"]
    distance = []
    for activity in activity_list:
        file_name='meta_'+activity+'.txt'
        with open(file_name,'r') as f:
            list_row=f.readline()
            column_list = eval(list_row)
            for sub_folder in column_list:
                fold = str(sub_folder[0])
                for sub in sub_folder[1:]:
                    file_list = []
                    sub_name=str(sub)
                    object_name = []
                    name = "dataset/sub" + fold + "/" + activity + "/" + sub_name + "_obj"
                    flag=1
                    # for i in range(numOfobject):
                    while flag:
                        if os.path.isfile(name+str(flag)+".txt"):
                            file_list.append(name+str(flag))
                            if len(object_name)==0:
                                object_name.append('a')
                            else:
                                object_name.append(chr(ord(object_name[-1])+1))
                        else:
                            break
                        flag+=1

                    print(object_name)
                    print(file_list)
                    dic=obj_form(object_name)
                    output=[]
                    for obj in dic:
                        points1=kal(file_list[object_name.index(obj[0])])
                        points2=kal(file_list[object_name.index(obj[1])])

                        if len(points1[0])!=len(points2[0]):
                            print("error 7")
                            exit()
                        for t in range(len(points1[0])):
                            dis=math.sqrt((points1[0][t]-points2[0][t])**2+(points1[1][t]-points2[1][t])**2)
                            label=""
                            if dis<=68:
                                label=0
                            elif 68<dis<452:
                                label=1
                            elif dis>=452:
                                label=2
                            else:
                                print("error 8")
                                exit()
                            output.append(QSR_structure(int(t), label, obj[0], obj[1]))
                            distance.append(dis)
                    output_world_qsr_trace(object_name,"result/"+"pl_dis/"+activity+"/"+"dis_",sub_name,activity,output)





