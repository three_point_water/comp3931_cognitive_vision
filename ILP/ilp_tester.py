

## this is the run file for version 2

import sys
from tree          import *
from queue_search  import *
from ilp_cover  import *
from data import Event,Group


def read(f,type,lists):
    list_row = f.readlines()
    idx = -1

    for i in range(len(list_row)):
        column_list = eval(list_row[i])
        tmp=None
        if type==0:
            tmp = Event(column_list[0], column_list[1],column_list[2],column_list[3])
            if column_list[0] == 0:
                # if i % 4 == 0:
                idx += 1
                lists.append([])
                lists[idx].append(tmp)
                continue
        # temporal
        elif type==1:
            tmp = column_list
            if i % 4 == 0:
                idx += 1
                lists.append([])
                lists[idx].append(tmp)
                continue
        #groups
        elif type==2:
            tmp = Group(i%3,column_list[0],column_list[1])
            if i % 3 == 0:
                idx += 1
                lists.append([])
                lists[idx].append(tmp)
                continue

        lists[idx].append(tmp)

    f.close()

    # return lists
def filter_same_results():
    final_result=[]
    for i,example in enumerate(results):
        for id,rules in reversed(list(enumerate(example))):
            match_list = [[] for _ in range(3)]
            for rule in rules:
                e=events[i][rule]
                match_list[e.group].append(e.rcc)
            if match_list in final_result:
                del results[i][id]
            else:
                final_result.append(match_list)

f_events = open("events.txt", "r")
f_groups = open("groups.txt", "r")
f_rels = open("relations.txt", "r")
f_meta=eval(open("meta_group.txt","r").readline())

print(f_meta)
# exit()
events,groups,relations=[],[],[]
read(f_events,0,events)
read(f_rels,1,relations)
read(f_groups,2,groups)
results=[[] for _ in range(len(events))]

# original = sys.stdout
# sys.stdout = open('tester_outcome.txt', 'w')

def zero_heuristic(state):
    return 0
#
# for i in range(len(events)):
#     search(make_ilp_problem(e=events, g=groups, r=relations, id=i), ('A_star', zero_heuristic), 5000, [])
for i in range(len(events)):
    search(make_ilp_problem(e=events, g=groups, r=relations, id=i,re=results[i]), ('A_star', zero_heuristic), 5000, [])


filter_same_results()


def rcc_decode(id):
    return {
        0: "dc",
        1: "po",
        2: "o",
    }.get(id, -1)

def allen_decode(index):
    return {
        1:"before",
        4:"overlap",
        2:"start",
        3:"during",
    # index+1 since the for loop start from 0
    }.get(index,-1)


def output(meta):
    var="V"
    f=open("output.txt","w")
    for id,rules in enumerate(results):
        for rule in rules:
            obj_names = ""
            last_var=rule[-1]
            first_var=rule[0]
            for obj in range(len(meta)):
                obj_names += "V" +str(last_var+obj+1)+","
            sur = "cereal("+obj_names+ "V"+str(first_var)+",V"+str(last_var)+"):-"
            content=""
            # spatial relations
            for e in rule:
                event=events[id][e]
                if content!="":
                    content+=","
                obj_name=""
                for obj in meta[event.group]:
                    obj_name+="V"+str(last_var+obj+1)+","
                content+=rcc_decode(event.rcc)+"("+obj_name+var+str(e)+")"
            # temporal relations
            for rel_id,rel in enumerate(relations[id]):
                for rel_list in rel:
                    if rel_list[0] in rule and rel_list[1] in rule:
                        content+=","+allen_decode(rel_id+1)+"("+var+str(rel_list[0])+","+var+str(rel_list[1])+")"
            content+="."
            f.writelines(sur+content+"\n")

            # print(sur+content)




output(f_meta)



#
#
