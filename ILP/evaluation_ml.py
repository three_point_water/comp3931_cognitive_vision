

# this file is used to do the evaluation for ML methods

from clyngor import ASP, solve
import os
dic={0:"cleaning_objects",1:"microwaving_food",2:"making_cereal",3:"stacking_objects",4:"unstacking_objects",5:"no predication made"}

# initial the rank table
level={"cleaning_objects":0,"microwaving_food":0,"making_cereal":2,"stacking_objects":2,"unstacking_objects":2}

def test():
    # notmp
    # rules="cleaning_objects(V2,V4):-dc(A,B,V2),po(A,B,V3),dc(A,B,V4)."
    # rules+="microwaving_food(V0,V2):-dc(A,B,V0),po(A,B,V1),o(A,B,V2)."
    # rules+="making_cereal(V1,V4):-po(A,C,V1),dc(A,B,V3),dc(B,C,V4). "
    # rules+="stacking_objects(V3,V11):-dc(B,C,V3),dc(C,D,V7),po(A,B,V9),dc(A,B,V11)."
    # rules+="unstacking_objects(V12,V17):-po(C,D,V12),dc(D,E,V13),po(D,E,V17)."

    # dis
    # rules="cleaning_objects(V0,V2):-short(A,B,V0),medium(A,B,V1),short(A,B,V2),before(V0,V1),before(V1,V2)."
    # rules+="microwaving_food(V0,V2):-medium(A,B,V0),short(A,B,V1),medium(A,B,V2),before(V0,V1),before(V1,V2)."
    # rules+="making_cereal(V0,V2):-medium(A,B,V0),medium(A,C,V1),medium(B,C,V2),overlap(V0,V1),overlap(V0,V2),overlap(V1,V2)."
    # rules+="stacking_objects(V0,V11):-medium(C,D,V0),short(C,D,V1),medium(A,C,V4),medium(B,D,V11),before(V0,V1),during(V1,V4),during(V1,V11)."
    # rules+="unstacking_objects(V9,V17):-short(C,D,V9),short(D,E,V14),medium(D,E,V17),before(V14,V17),start(V9,V14)."



    rules="cleaning_objects(V2,V4):-dc(A,B,V2),po(A,B,V3),dc(A,B,V4),before(V2,V3),before(V3,V4)."
    rules+="microwaving_food(V0,V2):-dc(A,B,V0),po(A,B,V1),o(A,B,V2),before(V0,V1),before(V1,V2)."
    rules+="making_cereal(V0,V3):-dc(A,C,V0),po(A,C,V1),dc(A,B,V3),before(V0,V1),start(V0,V3),during(V1,V3)."
    rules+="stacking_objects(V1,V19):-po(C,D,V1),dc(A,D,V4),dc(C,D,V7),dc(A,C,V19),before(V1,V7),overlap(V4,V7)."
    rules+="unstacking_objects(V11,V18):-dc(A,C,V11),dc(C,E,V14),po(C,E,V18),before(V14,V18),overlap(V11,V14)."

    # kal
    rules="cleaning_objects(V0,V2):-po(A,B,V0),dc(A,B,V1),po(A,B,V2),before(V0,V1),before(V1,V2)."
    rules+="microwaving_food(V0,V2):-dc(A,B,V0),po(A,B,V1),o(A,B,V2),before(V0,V1),before(V1,V2)."
    rules+="making_cereal(V0,V6):-dc(A,C,V0),po(A,C,V1),dc(A,C,V5),dc(B,C,V6),before(V0,V1),before(V1,V5),during(V1,V6),during(V5,V6)."
    rules+="stacking_objects(V0,V17):-dc(B,D,V0),po(B,D,V8),dc(A,D,V10),dc(A,C,V17),before(V0,V8),during(V8,V17),overlap(V8,V10)."
    rules+="unstacking_objects(V11,V16):-dc(D,E,V11),dc(C,E,V12),po(C,E,V16),before(V12,V16),during(V11,V12)."

    #ilasp
    # rules="cleaning_objects(V0, V4) :- before(V3, V4); dc(A, B, V4); po(A, B, V3); dc(A, B, V0)."
    # rules+="making_cereal(V0, V4) :- during(V1, V4); start(V0, V4); before(V0, V1); dc(B, C, V4); po(A, C, V1); dc(A, C, V0)."
    # rules+="microwaving_food(V0, V3) :- before(V0, V1); dc(A, B, V3); po(A, B, V1); dc(A, B, V0)."

    for key in dic.keys():
        if key==5:
            continue
        # for all files in table
        rootDir="result_kal/pl/"+dic[key]
        for lists in os.listdir(rootDir):
            path = os.path.join(rootDir, lists)
            if path[-2:]!="pl":
                continue
            f=open(path,"r")
            fp=f.readlines()
            content=""
            for c in fp:
                content+= c+"\n"
            content+=rules
            answers = ASP(content)
            repeat_list=[]
            for answer in answers:
                for ans in answer:
                    for head in dic.values():
                        if ans[0]==head and ans[0] not in repeat_list:
                            repeat_list.append(ans[0])

            if len(repeat_list)==0:
                # no predication made
                confustion_matrix[key][5]+=1
                continue

            # assign the ranks
            rank=0
            if len(repeat_list)!=1:
                for i in range(len(repeat_list)):
                    if level[repeat_list[i]]>rank:
                        rank=level[repeat_list[i]]
            else:
                rank=level[repeat_list[0]]


            for result in repeat_list:
                if level[result]==rank:
                    for i in dic.keys():
                        if dic[i]==result:
                            confustion_matrix[key][i]+=1
            # print(confustion_matrix)





# head="unstacking_objects"
# f = open("result_notmp/pl/" + head + "/output2.txt", "r").readlines()
confustion_matrix=[[0 for _ in range(6) ]for _ in range(5)]
test()
for i in confustion_matrix:
    print(i)
# dim=len(confustion_matrix[0])
# tp = [0 for _ in range(dim-1)]
# row = [0 for _ in range(dim)]
# col = [0 for _ in range(dim)]
# recall = 0
# accuary = 0
#
# for i in range(dim):
#     tp[i] = matrix[i][i]
#     for k in range(dim):
#         row[i]+= matrix[i][k]
#         col[i]+=matrix[k][i]
#
# for i in range(dim):
#     accuary+=tp[i]/col[i]
#     recall+=tp[i]/row[i]
# accuary=accuary/dim
# recall=recall/dim
# print(accuary)
# print(recall)
