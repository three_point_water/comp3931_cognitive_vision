
#this file is used to output

import itertools

# the last servel index of name are id (eg person1000)
# after pop , what left is type  (person)
def name2type(name):
    temp=list(name)
    # if last index is digit, pop it
    while temp[-1].isdigit():
        temp.pop(-1)
    return "".join(temp)

def rcc_decode(id):
    return {
        0: "dc",
        1: "po",
        2: "o",
    }.get(id, -1)
# fixed prefix
# define form of body and hear
def write_prefix(f,obj_types):
    time_name=["before","start","overlap","finish","during","meet"]
    sptial_name=["dc","po","o"]

    # all possible permutations include repeatation
    type_groups = list(itertools.permutations(obj_types, 2))
    for i in obj_types:
        # repeatation
        type_groups.append([i,i])

    content="#maxv(4).\n"
    for name in time_name:
        content+="#modeb(3,"+name+"(var(intv),var(intv))).\n"

    for name in sptial_name:
        for type in type_groups:
            content += "#modeb(3," + name + "(var("+type[0]+"),var("+type[1]+"))).\n"
    f.writelines(content)

def write_pl(events, relations,sub_name,head):
    f=open("result/pl/"+head+"/"+sub_name+".pl","w")
    # print(sub_name)
    prolog_format(events, f, relations)
    f.close()


def write_ILASP(events, relations, unique_time, fn, sub_name,activity,obj_names):
    f = open(fn, "a+")
    # add constant for sptial relations
    # prefix_FastLas(events, f, unique_time)

    # prefix = write_prefix(f,type_unique)
    names_list=""
    num_events=len(events)
    for obj in obj_names:
        names_list += obj + ","

    pre="#pos(eg"+str(sub_name)+"@1"+",{"+activity+"("+"t0,t"+str(num_events-1)+")},{}, {\n"
    # pre = "#pos(eg" + str(sub_name) + "@1" + ",{" + activity + "(" + names_list + "t0,t" + str(
        # num_events - 1) + ")},{}, {\n"
    f.writelines(pre)
    # add sptial relations
    prolog_format(events, f, relations)
    post = "}).\n"
    f.writelines(post)
    f.close()


def prefix_FastLas(events, f, unique_time):
    name_unique = []
    type_unique = []
    for e in events:
        type1 = name2type(e.obj1)
        type2 = name2type(e.obj2)
        if e.obj1 not in name_unique:
            # form   #constant(person,person221).
            content1 = "#constant(" + type1 + "," + e.obj1 + ").\n"
            name_unique.append(e.obj1)
            f.writelines(content1)
        if e.obj2 not in name_unique:
            content2 = "#constant(" + type2 + "," + e.obj2 + ").\n"
            name_unique.append(e.obj2)
            f.writelines(content2)
        if type1 not in type_unique:
            type_unique.append(type1)
        if type2 not in type_unique:
            type_unique.append(type2)
    # add constant for temporal relations
    for intv in unique_time:
        # form   #constant(intv,T1011).
        content = "#constant(intv," + "T" + str(intv) + ").\n"
        f.writelines(content)


def prolog_format(events, f, relations):
    for e in events:
        # form:   dc(obj1,obj2,T3),
        content = rcc_decode(e.rcc) + "(" + e.obj1 + "," + e.obj2 + "," + "t" + str(e.id) + ").\n"
        # only two objects  dc(t3)
        # content = e.rcc + "(" +"t" + str(e.id) + ").\n"
        f.writelines(content)
    for idx in range(len(relations)):
        for t in relations[idx]:
            if t != []:
                # get name of allen relations
                relation_name = allen_decode(idx + 1)
                if not relation_name:
                    print("error")
                    print(idx, t)
                content = relation_name + "(" + "t" + str(t[0]) + "," + "t" + str(t[1]) + ").\n"
                f.writelines(content)
    # if only 2 object
    # add temporal relations
    # for idx in range(len(events)-1):
    #     # get name of allen relations
    #     relation_name="before"
    #     content=relation_name+"("+"t"+str(idx)+","+"t"+str(idx+1)+").\n"
    #     f.writelines(content)


# decode
# index-> relations' names
# return -1 if error
def allen_decode(index):
    return {
        1:"before",
        4:"overlap",
        2:"start",
        3:"during",
    # index+1 since the for loop start from 0
    }.get(index,-1)
def allen_decode_2(index):
    return {
        1:"before",
        2:"meet",
        3:"overlap",
        4:"start",
        5:"during",
        6:"finish",
    # index+1 since the for loop start from 0
    }.get(index,-1)


def write_files(events,relations,groups,head):
    f_events = open(head+"_events.txt", "a+")
    f_groups = open(head+"_groups.txt", "a+")
    f_rels = open(head+"_relations.txt", "a+")
    # f_metag = open(head+"_meta_group.txt", "w")
    name=[]
    # group_info=[[] for _ in range(3)]
    for i in events:
        content = str([i.id,i.rcc,i.group,i.order,i.obj1,i.obj2])+'\n'
        f_events.writelines(content)
        # if len(group_info[i.group])==0:
        #     if i.obj1 not in name:
        #         name.append(i.obj1)
        #     if i.obj2 not in name:
        #         name.append(i.obj2)
        #     group_info[i.group].append(name.index(i.obj1))
        #     group_info[i.group].append(name.index(i.obj2))

    f_events.close()

    # f_metag.writelines(str(group_info))
    # f_metag.close()
    flag=777
    f_groups.writelines(str(flag))
    f_groups.writelines("\n")
    for i in groups:
        content=str([i.member,i.member_alt])+'\n'
        f_groups.writelines(content)

    f_groups.close()

    for i in relations:
        content=str(i)+"\n"
        f_rels.writelines(content)
    f_rels.close()
