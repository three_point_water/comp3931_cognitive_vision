
# this file is used to do the Kalman filter and cleanning data
from xml.etree import ElementTree as ET
import numpy as np
from qsrlib_io.world_trace import Object_State
from kf import EKF

class TrackerEKF(EKF):

    def __init__(self):
        EKF.__init__(self, 2, 2, pval=1, qval=0.001, rval=0.1)

    def f(self, x):
        # State-transition function is identity
        return np.copy(x)

    def getF(self, x):
        # So state-transition Jacobian is identity matrix
        return np.eye(2)

    def h(self, x):
        # Observation function is identity
        return x

    def getH(self, x):
        # So observation Jacobian is identity matrix
        return np.eye(2)


# this file is used to deal with mindeye dataset
def Read_XML(filename,world):

    tree = ET.parse(filename)
    root = tree.getroot()
    obj_name_list=[]
    for child in root[1][0]:
        # i have to say it is a silly name(prefix) in xml tree, cost me much time to find it
        if child.tag=="{http://lamp.cfar.umd.edu/viper#}object":

            id = child.get('id')
            name = child.get("name")
            if name=='Vehicle' :
                continue
            obj_name=name+str(id)
            obj_name_list.append(obj_name)
            obj = []
            for bbox in child[0]:
                x = bbox.get("x")
                y = bbox.get("y")
                height = bbox.get("height")
                width = bbox.get("width")

                # change framespan to str and then split by :
                # eg:  input:1:18    output:  frame[0]=1  frame[1]=18
                frame = str(bbox.get("framespan")).split(":")
                for time in range(int(frame[0]),int(frame[1])+1):
                    obj.append(Object_State(name=obj_name,timestamp=time,x=int(x),y=int(y),xsize=int(width), ysize=int(height)))

            world.add_object_state_series(obj)

    return obj_name_list,world


def kal(f):
    error_count=0
    kalfilt = TrackerEKF()
    measured_points = []  # measured
    output_points = [[] for _ in range(4)]
    raw_points=[[] for _ in range(4)]

    file = open(f + ".txt", "r")
    list_row = file.readlines()
    for i in range(len(list_row)):
        column_list = eval(list_row[i])
        x = column_list[2]
        y = column_list[3]
        if x==0 or y==0:
            error_count+=1
            continue
        raw_points[0].append(x)
        raw_points[1].append(y)
        measured = (x, y)
        measured_points.append(measured)
        # Update the Kalman filter with the mouse point, getting the estimate.
        estimate = kalfilt.step((x, y))
        # Add the estimate to the trajectory
        estimated = [int(c) for c in estimate]
        if i <20:
            output_points[0].append(x)
            output_points[1].append(y)
        else:
            output_points[0].append(estimated[0])
            output_points[1].append(estimated[1])

    for i in range(len(list_row)):
        column_list = eval(list_row[i])
        x = column_list[4]
        y = column_list[5]
        if x==0 or y==0:
            continue
        measured = (x, y)
        raw_points[2].append(x)
        raw_points[3].append(y)
        measured_points.append(measured)
        # Update the Kalman filter with the mouse point, getting the estimate.
        estimate = kalfilt.step((x, y))
        # Add the estimate to the trajectory
        estimated = [int(c) for c in estimate]
        if i <20:
            output_points[2].append(x)
            output_points[3].append(y)
        else:
            output_points[2].append(estimated[0])
            output_points[3].append(estimated[1])
    file.close()

    # code for visualation
    # k=range(len(list_row)-error_count)
    # ax=pl.subplot(projection="3d")
    # pl.plot(k,output_points[0],output_points[1],label="kal")
    # pl.plot(k,raw_points[0], raw_points[1], label="raw")
    # pl.legend(["kal","raw"])
    # ax.set_xlabel("time")
    # ax.set_ylabel("x")
    # ax.set_zlabel("y")
    # pl.show()

    return output_points


def Read_CAD_obj(file_list,objects_name,world):
    for i in range(len(file_list)):
        points=kal(file_list[i])
        obj=Read_CAD(points,objects_name[i])
        world.add_object_state_series(obj)

    return world


def Read_CAD(points,obj_name):
    obj = []
    for i in range(len(points[0])):
        # column_list = eval(list_row[i])
        x=points[0][i]
        y=points[1][i]
        w=points[2][i]-x
        h=points[3][i]-y
        if x==0 or y==0 or w==0 or h==0:
            continue
        try:
            obj.append(Object_State(name=obj_name, timestamp=i+1, x=int(x), y=int(y), xsize=int(w), ysize=int(h)))
        except:
            print(i)
            exit()

    return obj


def Read_list(filename):
    file = open(filename + ".txt", "r")
    list_row = file.readlines()
    list_source = []
    for i in range(len(list_row)):
        column_list = eval(list_row[i])
        # w = column_list[-2]
        # h = column_list[-1]
        # column_list[-2] = w / 2 + column_list[2]
        # column_list[-1] = h / 2 + column_list[3]
        # column_list[2]=column_list[2]-w/2
        # column_list[3]=column_list[3]-h/2
        list_source.append(column_list[2:])
        # column_list = list_row[i].strip().split("\t")
        # list_source.append(eval(column_list))
    file.close()
    return list_source