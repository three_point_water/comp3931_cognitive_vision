# -*- coding: utf-8 -*-
"""
this file is used to do ilp task for version 2
"""

from __future__ import print_function

print("Loading ilp_cover.py")
from copy import deepcopy

'''
state represatation
state=(length,rules,[the examples have been covered],[possible_paths])
action=(add a literal)

[_ for i in len(length of examples)]
The examples will not be covered if the previous already not covered

rules
[rule1 , rule 2]

'''


def ilp_print_problem_info():
    print("starting ")


# intial state all examples can be corvered
def ilp_get_initial_state():
    return [0, [], [1 for _ in range(length_examples)], [[[] for _ in range(g_len)] for _ in range(length_examples)]]


# return a list
# all possilble next queen location
def ilp_possible_actions(state):
    if state[0] == 0:
        return qc_initial_addition()
    else:
        return ilp_following_addition(state)


# all possible next literal
def qc_initial_addition():
    next = [i for i in range(length_event)]
    return next


# dc 0
# po 1
# o  2
def ilp_following_addition(state):
    # add method to stop if covered pos examples are less than a threshold
    current_id = state[1][-1]
    next = []
    current_event = events[target][current_id]
    # if state[0]:
    #     pass
    if current_id + 1 < length_event:
        for i in range(current_id + 1, length_event):
            next_event = events[target][i]
            next_group = next_event.group
            # if in the same group, compare directly
            if current_event.group == next_group:
                if current_event.rcc + 1 == next_event.rcc \
                        or current_event.rcc - 1 == next_event.rcc:
                    next.append(i)
            # if not, seach for the node with different group
            else:
                # if the next event already first one in the group
                if next_event.order == 0:
                    next.append(i)
                # if not, find the previous event of next event
                else:
                    max_order=-1
                    for idx in state[1]:
                        e=events[target][idx]
                        if e.group==next_group:
                            if e.order>max_order:
                                max_order=e.order

                    rcc = groups[target][next_group].member_alt[max_order]
                    if rcc + 1 == next_event.rcc or rcc - 1 == next_event.rcc:
                        next.append(i)

    return next


# return correct id
def find_index(word, list):
    return [i for i, c in enumerate(list) if c == word]


def calculate_tmp_dis(flag_match, flag_path):
    # if their temporal relations are same, return 1 direcly
    # here note that it is possible that two flags are -1
    # this situation should pass the check
    if flag_match == flag_path:
        return 1
    # if one of the relation are lack
    elif flag_match == -1 or flag_path == -1:
        return 0
    # if the temporal distance are not the same but acceptable
    elif flag_path - 1 == flag_match or flag_path + 1 == flag_match:
        return 2
    else:
        return 0


# 1. find the the added litera's group
# 2. in this group, form all possible paths

# macthed_list: current example
# example_id: the example need to cover
def find_pattern(state, match_list, example_id):
    new_rule = events[target][state[1][-1]]
    group_id = new_rule.group
    # [ [],[],[] ]
    #dc po o
    rcc_list = [[] for _ in range(3)]

    # create the rcc list for other pos example
    # try:
    for i, c in enumerate(groups[example_id][group_id].member_alt):
        rcc_list[c].append(groups[example_id][group_id].member[i])


    # compare the spatial relations in this pos examples
    possible_path = state[3][example_id]

    # in the first iteration of each group
    if len(possible_path[group_id]) == 0:
        # if the group in the example also empty
        if len(groups[example_id][group_id].member)==0:
            return 1, possible_path
        #if not empty
        for i in rcc_list[new_rule.rcc]:
            possible_path[group_id].append([i])
    # if current rule's length is longer than pos example
    elif len(possible_path[group_id][0])>=len(groups[example_id][group_id].member):
        return -1,possible_path
    else:
        path_tmp = []
        for i, path2 in enumerate(possible_path[group_id]):
            m = path2[-1]
            for n in rcc_list[new_rule.rcc]:
                if m < n:
                    tmp = deepcopy(possible_path[group_id][i])
                    tmp.append(n)
                    path_tmp.append(tmp)
        # if no one satisfy, path temp is empty
        possible_path[group_id] = path_tmp



    # if the possbile path is empty, return 0
    if len(possible_path[group_id]) == 0:
        return -1, possible_path


    if g_len==1:
        return 1,possible_path
    # find temporal relations
    # compare the temporal relations with all other different groups
    # here we do not need to comapre within the same group

    # possible_path struct is the same as match list
    main_rels = relations[example_id]
    match_rels = relations[target]

    # for other 2 groups
    loop = range(3)
    loop.remove(group_id)

    max_score = [[[]for _ in range(3)]for _ in range(len(possible_path[group_id]))]
    record= [[[]for _ in range(3)]for _ in range(len(possible_path[group_id]))]

    # for all paths in same groups
    for id1, path1 in enumerate(possible_path[group_id]):
        n_p = path1[-1]
        n_q = match_list[group_id][-1]
        for other_group in loop:
            # score=[0 for _ in range(len(possible_path[other_group]))]
            # if not none
            if possible_path[other_group] != []:
                # for one path in this group
                # if any one path passes all check, it will return 1
                # score is the number of paths in one other group
                maxScore = -100
                # for all paths in other groups
                for id2, path2 in enumerate(possible_path[other_group]):
                    score=2
                    if len(path2) != len(match_list[other_group]):
                        print("error")
                        print(other_group)
                        print(example_id)
                        print(possible_path)
                        print(match_list)
                        print(state)
                        print(target)
                        # print(result)
                        exit()
                    # for each event in the path
                    for e in range(len(path2)):
                        p = path2[e]
                        q = match_list[other_group][e]
                        flag_path = -1
                        flag_match = -1
                        # if n is the bigest , not fit the temporal relations
                        if n_q<q or n_p<p:
                            flag_match=-10
                        else:
                            # idx is index of tmp relations
                            for idx1, tem_rel1 in enumerate(main_rels):
                                # if find exist a relation
                                if [p, n_p] in tem_rel1:
                                    flag_path = idx1
                                    break

                            # search in the match list
                            for idx2, tem_pel2 in enumerate(match_rels):
                                if [q, n_q] in tem_pel2:
                                    flag_match = idx2
                                    break

                        # here note that it is possible that all flags are -1
                        # this situation should pass the check
                        tmp = calculate_tmp_dis(flag_match, flag_path)
                        if tmp == 1:
                            # check next event
                            continue
                        # if the temporal relation not same but acceptable
                        elif tmp == 2:
                            score-= 1
                        else:
                            # this path not pass the check
                            # check next path
                            score-=2
                    # if state[1]==[4,5]:
                    #     print(score)
                    if score>maxScore:
                        maxScore=score
                        record[id1][other_group]=[id2]
                    elif score==max_score[id1][other_group]:
                        record[id1][other_group].append(id2)
                max_score[id1][other_group] = maxScore
            else:
                max_score[id1][other_group]=2
            # only keep the max score path

            # tmp = max(score[other_group])
            # if state[1] == [2, 3, 5]:
            #     print(score[other_group])
            # if tmp<0:
            #     possible_path[other_group]=[]
            #     continue
            # for p in reversed(range(len(possible_path[other_group]))):
            #     if score[other_group][p] < tmp:
            #         del possible_path[other_group][p]
            #         del score[other_group][p]

        # if possible path in this group is empty

            # continue

    # after check all path in this group
    scores=[]
    for i in range(len(possible_path[group_id])):
        tem=0
        for k in range(3):
            if k!=group_id:
                try:
                    tem+=max_score[i][k]
                except:
                    print(max_score)
                    exit()
        scores.append(tem)

    # find max results
    final_id=[]
    max_s=max(scores)
    for i in range(len(possible_path[group_id])):
        if scores[i]==max_s:
            final_id.append(i)

    tem2=[]
    for i in final_id:
        tem2.append(possible_path[group_id][i])
    possible_path[group_id]=tem2

    final_id2=[[] for _ in range(3)]
    for i in final_id:
        for k in loop:
            for p in record[i][k]:
                if p not in final_id2[k]:
                    final_id2[k].append(p)

    for i in loop:
        tem=[]
        for id2 in final_id2[i]:
            tem.append(possible_path[i][id2])
        possible_path[i]=tem

    # return results
    for i in final_id:
        if scores[i]>=3:
            return 1,possible_path
        elif scores[i]>1:
            return -2, possible_path
        else:
            return -1, possible_path

    #
    # for other_group in loop:
    #     for k in score[other_group]:
    #         if k <=0:
    #             # the temporal relation in this group not satify by this pos example
    #             return -1, possible_path
    #         elif k==1:
    #             return -2, possible_path
    # # if can reach here
    # # means pass all three group check
    # return 1, possible_path


#
def get_score(state):
    # get stacked rules
    rules = state[1]
    match_list = [[] for _ in range(3)]
    for i in rules:
        g_id = events[target][i].group
        match_list[g_id].append(i)

    cover_example = state[2]
    possible_paths = state[3]
    # check relations
    for example_id in loop_list:
        # if this example is covered in parent node
        if cover_example[example_id] > 0.7:
            tmp, possible_path = find_pattern(state, match_list, example_id)
            # this example is not covered
            if tmp == -1:
                cover_example[example_id] = 0
            elif tmp == -2:
                cover_example[example_id] -= 0.2
            possible_paths[example_id] = possible_path

    # calculate the total score
    score = 0
    for i in cover_example:
        score += i
    # if less a threshold
    # set length -1

    if score < len(cover_example) / 2:
        state[0] = -1
    else:
        state[0] += 1

    state[2] = cover_example
    state[3] = possible_paths
    # print(state[1])
    # print(state[2])
    # print(state[3])
    # print(state[0])
    # if state[1] == [1,2,5,6]:
        # print(p)
        # print(q)
        # print(n)
        # exit()

    # print("\n")


def ilp_initial_successor(action):
    new_state = deepcopy(ilp_initial_state)
    # update action first
    new_state[1].append(action)
    get_score(new_state)
    return new_state


def ilp_successor_state(action, state):
    if state[0] == 0:  # initial state
        return ilp_initial_successor(action)
    # how to deal with empty
    new_state = deepcopy(state)
    new_state[1].append(action)
    get_score(new_state)
    # new_state[0] += 1
    return new_state


def sum(cover_examples):
    score = 0
    for i in cover_examples:
        score += i
    return score


# in fact, there is no goal
# but still can set a expected acceptable result
def ilp_goal_state(state):

    ideal_result=[1 for _ in range(length_examples)]
    score=0
    for i in range(length_examples):
        score+=abs(ideal_result[i]-state[-2][i])

    if state[0]>len(events[target])/2:
        if score<1:
            print(state)
            result.append(state[1])

    return False


def make_ilp_problem(e, g, r, id,re):
    global events, groups, relations, ilp_initial_state, target,result
    global length_examples, length_event, max_events, loop_list,g_len
    # g_len=len(groups[0])
    target = id
    events = e
    groups = g
    relations = r
    result=re
    length_examples = len(groups)

    g_len = len(groups[0])
    length_event = len(events[target])
    max_events = max(len(k) for k in events)

    # we will search for all other pos examples except target one
    loop_list = range(length_examples)
    loop_list.remove(target)

    ilp_initial_state = ilp_get_initial_state()

    return (None,
            ilp_print_problem_info,
            ilp_initial_state,
            ilp_possible_actions,
            ilp_successor_state,
            ilp_goal_state
            )
