# -*- coding: utf-8 -*-
"""
this file is used to create hypo for ilasp
"""

from __future__ import print_function

print("Loading ilp_cover.py")
from copy import deepcopy

'''
state represatation
state=(length,rules)
action=(add a literal)
'''

def ilp_print_problem_info():
    print("The queen cover question ")

# intial state all examples can be corvered
def ilp_get_initial_state():
    return [0, []]


# return a list
def ilp_possible_actions(state):
    if state[0] == 0:
        return qc_initial_addition()
    else:
        return ilp_following_addition(state)


# all possible next literal
def qc_initial_addition():
    next = [i for i in range(length_event)]
    return next


# dc 0
# po 1
# o  2
def ilp_following_addition(state):
    current_id = state[1][-1]
    next = []
    current_event = events[current_id]
    if current_id + 1 < length_event:
        for i in range(current_id + 1, length_event):
            next_event = events[i]
            next_group = next_event.group
            # if in the same group, compare directly
            if current_event.group == next_group:
                if current_event.rcc + 1 == next_event.rcc \
                        or current_event.rcc - 1 == next_event.rcc:
                    next.append(i)
            # if not, seach for the node with different group
            else:
                # if the next event already first one in the group
                if next_event.order == 0:
                    next.append(i)
                # if not, find the previous event of next event
                else:
                    max_order=-1
                    for idx in state[1]:
                        e=events[idx]
                        if e.group==next_group:
                            if e.order>max_order:
                                max_order=e.order
                    rcc = groups[next_group].member_alt[max_order]
                    if rcc + 1 == next_event.rcc or rcc - 1 == next_event.rcc:
                        next.append(i)

    return next


#
def find_index(word, list):
    return [i for i, c in enumerate(list) if c == word]


def ilp_initial_successor(action):
    new_state = deepcopy(ilp_initial_state)
    # update action first
    new_state[0]+=1
    new_state[1].append(action)
    return new_state


def ilp_successor_state(action, state):
    if state[0] == 0:  # initial state
        return ilp_initial_successor(action)
    new_state = deepcopy(state)
    new_state[1].append(action)
    new_state[0] += 1
    return new_state


# in fact, there is no goal
def ilp_goal_state(state):
    length=0
    if length_event>15:
        length=len(events)/2
    elif length_event>10:
        length=length_event-3
    else:
        length = length_event
    if state[0]>2 and state[0]<length:
        result.append(state[1])
    return False


def make_ilp_problem(e, g, id,re):
    global events, groups, ilp_initial_state, target,result,length_event
    target = id
    events = e[target]
    groups = g[target]
    length_event = len(events)
    result=re
    ilp_initial_state = ilp_get_initial_state()
    return (None,
            ilp_print_problem_info,
            ilp_initial_state,
            ilp_possible_actions,
            ilp_successor_state,
            ilp_goal_state
            )
