#!/usr/bin/env python
# -*- coding: utf-8 -*-

# this file is used to generate attribute-value table

from __future__ import print_function, division
from qsrlib.qsrlib import QSRlib, QSRlib_Request_Message
from qsrlib_io.world_trace import World_Trace
import argparse
from data import QSR_structure,Event
import os
from preprocess import Read_CAD_obj
from writeILP import write_files,write_ILP,write_pl
import itertools
from utils import rcc_encode

def obj_form(obj_list):
    out = []
    for i in itertools.combinations(obj_list, 2):
        out.append(i)
    return out

def all_index(list,item):
    return [i for i,c in enumerate(list) if item==c]
def output_world_qsr_trace(obj_list, which_qsr, qsrlib_response_message,fn,sub_name,activity):
    obj_format=obj_form(obj_list)
    print(obj_format)
    events=[]
    dic = [[]for _ in range(len(obj_format))]
    # tmep_event = [[] for _ in range(len(obj_format))]
    for t in qsrlib_response_message.qsrs.get_sorted_timestamps():
        finish_list=[]
        update = 0
        for k, v in zip(qsrlib_response_message.qsrs.trace[t].qsrs.keys(),
                        qsrlib_response_message.qsrs.trace[t].qsrs.values()):
            temp=str(k).split(',')
            obj1=temp[0]
            obj2=temp[1]
            rcc=v.qsr.values()[0]
            ind=-1
            try:
                ind=obj_format.index((obj1,obj2))
            except:
                try:
                    ind = obj_format.index((obj2, obj1))
                except:
                    print(obj_format)
                    print("error 9")
                    exit()

            if ind==-1:
                print("error 2")
                exit()
            # this ground alread go through
            if ind in finish_list:
                continue

            finish_list.append(ind)
            # if t ==0 initial the dic list
            if t==1:
                dic[ind].append(t)
                dic[ind].append(rcc)
                dic[ind].append(-1)
                # ev=Event(int(t),rcc,obj_format[ind][0], obj_format[ind][1])
                # events.append(ev)
            else:
                if dic[ind][1]!=rcc:
                    update=1
                    dic[ind][2]=rcc

        if update:
            for ind in range(len(obj_format)):
                ev2=Event(dic[ind][0],dic[ind][1], obj_format[ind][0], obj_format[ind][1])
                ev2.end_time=t
                events.append(ev2)
                dic[ind][0]=t+1
                if dic[ind][2]!=-1:
                    dic[ind][1]=dic[ind][2]
                dic[ind][2]=-1
    output=[[]for _ in range(len(obj_format))]
    for i,c in enumerate(events):
        # for k in obj_format:
        ind=obj_format.index((c.obj1,c.obj2))
        rcc=rcc_encode(c.rcc)
        output[ind].append(rcc)
    # for i in output:
    #     print(i)

    stand_format = [('a', 'b'),('a', 'c'),('a', 'd'),('a','e'),
                    ('b', 'c'), ('b', 'd'), ('b', 'e'), ('c', 'd'),('c', 'e'),('d', 'e')]
    out = [[] for _ in range(len(obj_format))]
    col = []
    col_dic={}
    for k in range(1, 5):
        for i in itertools.product([0, 1, 2], repeat=k):
            col.append(i)
            col_dic[i]=[]
            # print(i)

    for ind in range(len(obj_format)):
        for i,c in enumerate(col):
            length=len(c)
            if length==1:
                col_dic[c]=all_index(output[ind],c[0])
            else:
                new_value = []
                for m in col_dic[c[:-1]]:
                    for ind2,n in enumerate(output[ind][m+1:]):
                        if n==c[-1]:
                            new_value.append(ind2+m+1)
                col_dic[c]=new_value
            out[ind].append(len(col_dic[c]))

    # for i in out:
    #     print(i)
    # print("\n")
    ou= [[0 for _ in range(len(col))] for _ in range(len(stand_format))]
    for i,c in enumerate(obj_format):
        ind=stand_format.index(c)
        ou[ind]=out[i]

    for i in ou:
        print(i)
    # print(len(col))

    return ou


if __name__ == "__main__":
    # create a QSRlib object if there isn't one already
    qsrlib = QSRlib()


    # parse command line arguments
    options = sorted(qsrlib.qsrs_registry.keys())
    # print(options)
    parser = argparse.ArgumentParser()
    parser.add_argument("qsr", help="choose qsr: %s" % options, type=str)
    args = parser.parse_args()
    if args.qsr in options:
        which_qsr = args.qsr
    else:
        raise ValueError("qsr not found, keywords: %s" % options)
    world = World_Trace()
    activity_list = {"unstacking_objects":-2, "cleaning_objects":-1, "stacking_objects":2, "making_cereal":0,
                     "microwaving_food":1}

    X=[]
    y=[]
    for activity in activity_list.keys():
        file_name = 'meta_' + activity + '.txt'
        with open(file_name,'r') as f:
            list_row=f.readline()
            column_list = eval(list_row)
            for sub_folder in column_list:
                fold = str(sub_folder[0])
                for sub in sub_folder[1:]:
                    world = World_Trace()
                    file_list = []
                    sub_name=str(sub)
                    object_name = []
                    name = "dataset/sub" + fold + "/" + activity + "/" + sub_name + "_obj"
                    flag=1
                    # for i in range(numOfobject):
                    while flag:
                        if os.path.isfile(name+str(flag)+".txt"):
                            file_list.append(name+str(flag))
                            if len(object_name)==0:
                                object_name.append('a')
                            else:
                                object_name.append(chr(ord(object_name[-1])+1))
                        else:
                            break
                        flag+=1
                    print(object_name)
                    print(file_list)
                    world= Read_CAD_obj(file_list, object_name, world)
                    dynammic_args = {"tpcc": {"qsrs_for": [("o1", "o2", 'o3')]}}
                    qsrlib_request_message = QSRlib_Request_Message(which_qsr, world, dynammic_args)
                    # request your QSRs
                    qsrlib_response_message = qsrlib.request_qsrs(req_msg=qsrlib_request_message)
                    # pretty_print_world_qsr_trace(which_qsr, qsrlib_response_message)

                    tmp=output_world_qsr_trace(object_name,which_qsr, qsrlib_response_message,"result/"+"pl/"+activity+"/"+"train",sub_name,activity)
                    X.append(tmp)
                    y.append(activity_list[activity])

    outfile1=r"data2"
    outfile2=r"label"
    import numpy as np
    # X = np.array(X)

    np.save(outfile1,X)
    np.save(outfile2,y)


