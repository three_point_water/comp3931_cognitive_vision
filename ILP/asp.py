
# this file is used to generate asp scripts
from clyngor import ASP, solve
import os
def test(rule,head,pre,result_list):
    # create the dic
    dic={0:"cleaning_objects",1:"microwaving_food",2:"making_cereal",3:"stacking_objects",4:"unstacking_objects"}
    score={0:[0,0],1:[0,0],2:[0,0],3:[0,0],4:[0,0]}

    for key in dic.keys():
        rootDir=pre+dic[key]
        total=0.0
        cor=0.0
        for lists in os.listdir(rootDir):
            path = os.path.join(rootDir, lists)
            if path[-2:]!="pl":
                continue
            total+=1
            f=open(path,"r")
            fp=f.readlines()
            content=""

            for c in fp:
                content+= c+"\n"
            content+=rule
            # generate the asp script
            answers = ASP(content)
            flag=1
            for answer in answers:
                for key2 in answer:
                    if head in key2[0]:
                        flag=0
                        cor+=1
                        break
                if not flag:
                    break
        score[key][0]=cor
        score[key][1]=total
    ind=-1
    for i in range(5):
        if dic[i]==head:
            ind=i

    # socing function
    scores=0
    for i in range(5):
        if i ==ind:
            scores+=score[i][0]*1.5
        else:
            if ind>=2:
                if i>=2:
                    scores-=score[i][0]*1.5
            else:
                scores-=score[i][0]

    result_list.append([rule,scores,score])


head="microwaving_food"
pre="result_kal/pl/"
result_list=[]
f = open(pre + head + "/output.txt", "r").readlines()
count=0
for i in f:
    test(i,head,pre,result_list)
    count+=1
    if count==500:
        break
    if count%100==0:
        print(count)

if len(f)>1000:
    for i in f[-500:-1]:
        test(i,head,pre,result_list)

result_list.sort(key=lambda x: x[1], reverse=True)
k=len(result_list)
if k>10:
    k=10
# select first 10 results
for i in result_list[:10]:
    print(i[0])
    print(i[1])
    print(i[2])
