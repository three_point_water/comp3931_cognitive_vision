# this file contains main data struct

class Event:
    def __init__(self,id,rcc,g,o,obj1,obj2):
        self.id = id
        self.group = g
        self.rcc = rcc
        self.order=o
        self.obj1=obj1
        self.obj2=obj2

class Group:
    def __init__(self,id,member,member_alt):
        self.id=id
        self.member=member
        self.member_alt = member_alt
